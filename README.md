No Game, No Soup!

Maybe we can work out a title from the word "undergrowth".
I'm thinking of something along the lines of "thicket", "unknown" and 
"chaotic", since as about dungeon delving.


"docs":

    Add to docs and the appropriate item as a means of reminding 
    yourself, or others; of things that need attention or that you want 
    to bring up for discussion. Add description to links.
    
    Add new file if a topic is missing (like bugs when we get them).
    
    This structure of a billboard, bookmarks and history-keeping will 
    most likely change later on.
    
    Until then we log our concerns and ideas in textfiles in docs.

    
"inspirations":

    This is intended to be a sort of mishmash/dumpster of images, 
    sounds, music and text for whatever we find that may be interesting 
    for our project. Just dump some shit here! =)