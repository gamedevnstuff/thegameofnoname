Modifier - Reference

"modifier/AREA.json":

    An area has rooms in it. It can be a dungeon, surface or static
    for example.
    
    This template contain base modifiers for area generation, such as
    multipliers for generating number of rooms, a definitive max limit
    and a list of room size limits.
    
    A room is a room in a dungeon, but also a "room" in an outdoor area;
    such as in "surface". The generator makes no difference between the
    different kind of areas, that's handled by these templates.
    
    For room calculation, a varying number is derived from "difficulty"
    and then added to the base number of rooms for the type of area.
    
    Room size definitions are also derived from here; small, medium or
    large rooms all have min and max "tile" limits. A tuple containing a
    lists. Tuple[0] maps to x-dimension tile-range, and tuple[1] maps to
    y-dimension.

    
"template/AREA.json":

    A dictionairy of dungeons.
    
    Entries ending with "...cave" indicate a base area without any added
    options.
    
    Each entry contain a reference to which tileset it uses and optional
    "options" object. Keys in "options" are meant to override 
    base-modifiers in AREA.json if conflicting, or to add additional
    control over how the dungeon in question will be generated. For 
    instance if there is a "subtileset" present it should tell the
    generator that some rooms should be painted with an alternative
    tileset. 
    
    "rooms" is meant to override room count generation with a specific 
    bounds-tuple. Ex: (2, 3) will generate a dungeon with 2 or 3 rooms, 
    and (1, 1) will generate one with only 1 room.