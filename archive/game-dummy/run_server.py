__author__ = 'meatpuppet'


import argparse

from game.network.host import Host
from game.util import log

parser = argparse.ArgumentParser()
parser.add_argument('--address', dest='address', default='0.0.0.0')
parser.add_argument('--port', dest='port', default=33333)
parser.add_argument('--loglevel', dest='loglevel', default='info')
args = parser.parse_args()

level=args.loglevel.upper()
logger = log.setup_custom_logger('root', level, to_file=False)

host = Host(args.address, args.port)

logger.info('listening on %s:%s...' % (args.address, args.port))
while(host.is_alive):
    host.update()

