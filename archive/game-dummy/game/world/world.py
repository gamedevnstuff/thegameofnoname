__author__ = 'meatpuppet'

from .generator import Generator
from .room import Room
from .entities.player import Player

class World():
    def __init__(self):
        self.is_active = False
        self.rooms = {}
        self.roomgenerator = Generator(Room)
        self.playergenerator = Generator(Player)
        pass

    def add_player(self):
        '''
        add player to world

        :param player:
        :return:
        '''
        p = self.playergenerator.new(Player, (0,0))
        self.is_active = True

    def remove_player(self, player):
        '''
        remove player from world

        :param player:
        :return:
        '''
        if len(self.playergenerator) == 0:
            self.is_active = False
        self.playergenerator.remove(player)
