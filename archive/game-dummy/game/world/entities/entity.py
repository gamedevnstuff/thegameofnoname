__author__ = 'meatpuppet'


class Entity():
    def __init__(self, coord=(0, 0)):
        self._coord = coord
        self.update_pending = False
        self.is_active = True
        pass

    """
    def _watch_updates(self):
        self.update_pending = True
        def dec(func):
            def wrap(coord_tuple):
                func(coord_tuple)
            return wrap
        return dec
    """

    @property
    def coord(self):
        return self._coord

    @coord.setter
    def coord(self, coord_tuple):
        self.update_pending = True
        self._coord = coord_tuple

    def get_coord_if_needed(self):
        if self.update_pending:
            return self.coord
        else:
            return None