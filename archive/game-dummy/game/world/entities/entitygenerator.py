__author__ = 'meatpuppet'

from game.world.generator import Generator
from .entity import Entity

class EntityGenerator(Generator):
    """
    an object of this class keeps track of all the entities it creates
    """

    def __init__(self, class_to_generate=Entity):
        super().__init__(class_to_generate)

    def new(self, coords=(0, 0), class_to_generate=Entity):
        entity = class_to_generate(self.id_cnt, coords)
        self.id_object_dict[self.id_cnt] = entity
        self.id_cnt += 1
        return entity

    def get_updated(self):
        """
        returns a list of all updated entities, setting them updated

        :return:
        """
        updated_entities = []
        for entity in self.id_object_dict.keys():
            if entity.update_pending:
                updated_entities.append(entity)
                entity.update_pending = False
