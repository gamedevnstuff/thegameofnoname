__author__ = 'meatpuppet'

from .entities.entitygenerator import EntityGenerator
from .entities.entity import Entity


class Room():
    def __init__(self):
        self.is_active = False
        self.players = []
        self.entitygenerator = EntityGenerator()
        self.map = None
        pass

    def add_player(self, player):
        '''
        add player to room

        :param player:
        :return:
        '''
        self.players.append(player)
        self.is_active = True

    def remove_player(self, player):
        '''
        remove player from room

        :param player:
        :return:
        '''
        self.players.remove(player)
        if len(self.players) == 0:
            self.is_active = False

    def get_updated_entities(self):
        return self.entitygenerator.get_updated()

    def get_map(self):
        return self.map
