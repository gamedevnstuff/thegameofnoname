__author__ = 'meatpuppet'


class Generator():
    def __init__(self, class_to_generate):
        self.class_to_generate = class_to_generate
        self.id_object_dict = {}
        self.id_cnt = 0

    def new(self):
        object = self.class_to_generate()
        self.id_object_dict[self.id_cnt] = object
        self.id_cnt += 1
        return object

    def __len__(self):
        return len(self.id_object_dict)

    def get_all(self):
        objects = []
        for obj in self.id_object_dict.values():
            objects.append(obj)
        return objects

    def get_active(self):
        objects = []
        for obj in self.id_object_dict.values():
            if obj.is_active:
                objects.append(obj)
        return objects

    def get_inactive(self):
        objects = []
        for obj in self.id_object_dict:
            if not obj.is_active:
                objects.append(obj)
        return objects

    def get_by_id(self, id):
        return self.id_object_dict[id]

    def get_id(self, object):
        for key, obj in self.id_object_dict.values():
            if obj == object:
                return key

    def remove(self, object):
        for key in self.id_object_dict.keys():
            if self.id_object_dict[key] == object:
                self.id_object_dict.__delitem__(key)
                return True
        return False
