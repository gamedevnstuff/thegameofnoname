__author__ = 'meatpuppet'

import enet

from game.world.generator import Generator
from game.world.world import World

import json

import logging
logger = logging.getLogger('root')

class Host():
    def __init__(self, address='0.0.0.0', port=33333):
        """
        creates a new host object and the first world object (thats inactive until someone joins)

        :param address:
        :param port:
        :return:
        """
        self.host = enet.Host(enet.Address(address, port), 10, 0, 0, 0)
        self.event = self.host.service(0)
        self.worldgenerator = Generator(World)
        self.playerid_peer_dict = {}
        self.is_alive = True

        # needed:
        # peer -> player+welt
        # welt -> peer+player
        self.clients = {}

        self.add_world()

        self.cmds = {
            'log_in': self.cmd_log_in,
        }
        logger.debug('created new host object')


    def add_world(self):
        self.worldgenerator.new()

    def rm_world(self, world):
        self.worldgenerator.remove(world)

    def sync_clients(self):
        for world in self.worldgenerator.get_active():
            for room in world.roomgenerator.get_active():
                updated_entities = room.get_updated_entities()
                players = room.players
                for player in players:
                    # TODO ab hier blödsinn...
                    player_id = room.entitygenerator.get_id(player)
                    peer = self.playerid_peer_dict[player_id]
                    # TODO: updated_entities senden

    def _send_reliable(self, peer, dict):
        j = json.dumps(dict)
        bytes = j.encode()
        peer.send(0, enet.Packet(bytes, enet.PACKET_FLAG_RELIABLE))

    def update(self):
        #logger.debug('update...')
        event = self.host.service(1000/45)
        if event.type == enet.EVENT_TYPE_CONNECT:
            logger.debug('connect: %s)' % event.peer.address)
            pass

        elif event.type == enet.EVENT_TYPE_RECEIVE:
            try:
                p_data = json.loads(event.packet.data.decode())
            except:
                p_data = ''

            cmd = p_data.get('cmd', None)
            if cmd in self.cmds:
                logger.debug('got cmd %s (%s)' % (cmd, str(p_data)))
                self.cmds[cmd](event, p_data)
            else:
                logger.debug('unknown packet: %s' % event.data.decode())
            pass

        elif event.type == enet.EVENT_TYPE_DISCONNECT:
            logger.debug('disconnect: %s' % event.peer.address)
            pass

        self.sync_clients()

    def _get_address_str(self, event):
        return str('%s:%s' % (event.peer.address.host, event.peer.address.port))

    def cmd_log_in(self, event, json_obj):
        username = json_obj.get('username', None)
        if self.clients.get(self._get_address_str(event), None) == None\
                and username:
            logger.debug('new client logged in: %s (%s)' % (username, self._get_address_str(event)))
            self.clients[self._get_address_str(event)] = {'username': username,
                                                          'player_obj': None}
            ret = {'cmd': 'log_in', 'ret': True}
            self._send_reliable(event.peer, ret)






