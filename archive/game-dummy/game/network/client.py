__author__ = 'meatpuppet'

"""
>>> import enet
>>> host = enet.Host(None, 1, 0, 0)
>>> peer = host.connect(enet.Address("localhost", 33333), 1)
"""
import enet
import json

import logging

logger = logging.getLogger('root')


class Client():
    def __init__(self, address='localhost', port=33333):
        self.host = enet.Host(None, 1, 0, 0, 0)
        self.peer = None
        self.loggend_in = False
        self.is_alive = False

        self.cmds = {
            'log_in': self.cmd_set_logged_in
        }

        self.address = address
        self.port = port

        pass

    def connect(self):
        self.peer = self.host.connect(enet.Address(self.address, self.port), 1)
        event = self.host.service(5000)  # wait 5 sec for connection
        if event.type == enet.EVENT_TYPE_CONNECT:
            self.is_alive = True
            return True
        else:
            return False

    def send_log_in(self, username):
        logger.debug('sending login')
        d = {'cmd': 'log_in',
             'username': username}
        self._send_reliable(d)

    def _send_reliable(self, dict):
        j = json.dumps(dict)
        bytes = j.encode()
        self.peer.send(0, enet.Packet(bytes, enet.PACKET_FLAG_RELIABLE))

    def update(self):
        event = self.host.service(1000 / 45)

        if event.type == enet.EVENT_TYPE_CONNECT:
            logger.debug('connect: %s)' % event.peer.address)

        elif event.type == enet.EVENT_TYPE_RECEIVE:
            try:
                p = json.loads(event.packet.data.decode())
            except:
                p = ''

            cmd = p.get('cmd', None)
            if cmd in self.cmds:
                logger.debug('got cmd %s (%s)' % (cmd, str(p)))
                self.cmds[cmd](event, p)
            else:
                logger.debug('unknown packet: %s' % event.data.decode())
            pass

        elif event.type == enet.EVENT_TYPE_DISCONNECT:
            logger.debug('disconnect: %s' % event.peer.address)
            pass

    def cmd_set_logged_in(self, event, json_obj):
        self.loggend_in = True
