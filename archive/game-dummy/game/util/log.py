__author__ = 'meatpuppet'
"""
http://stackoverflow.com/questions/7621897/python-logging-module-globally
"""

import logging

def setup_custom_logger(name, level=logging.DEBUG, to_file=True):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')


    if to_file:
        handler = logging.FileHandler('%s.log' % str(level))
    else:
        handler = logging.StreamHandler()

    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger



