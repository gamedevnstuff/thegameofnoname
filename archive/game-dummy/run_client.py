__author__ = 'meatpuppet'


import argparse

from game.network.client import Client
from game.util import log

parser = argparse.ArgumentParser()
parser.add_argument('--address', dest='address', default='0.0.0.0')
parser.add_argument('--port', dest='port', default=33333)
parser.add_argument('--loglevel', dest='loglevel', default='info')
parser.add_argument('--username', dest='username', default='my_name')
args = parser.parse_args()

level=args.loglevel.upper()
logger = log.setup_custom_logger('root', level, to_file=False)

client = Client(args.address, args.port)

logger.info('starting loop!')


if client.connect():
    logger.info('Connected! Sending Login...')
client.send_log_in(args.username)

while(client.is_alive):
    client.update()

