__author__ = 'meatpuppet'

from game.util import log
import logging
logger = log.setup_custom_logger('root', logging.DEBUG)

logger.debug('running tests...')

import unittest
tests = unittest.TestLoader().discover('tests')
unittest.TextTestRunner(verbosity=2).run(tests)
