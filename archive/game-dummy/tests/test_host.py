__author__ = 'meatpuppet'

import unittest


import game.network.client as c
import game.network.host as h

import threading

import logging
logger = logging.getLogger('root')

def _update(host):
    while(host.is_alive):
        host.update()


class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.host = h.Host('127.0.0.1', 33333)
        self.test_client = c.Client('127.0.0.1', 33333)

        self.host_thr = threading.Thread(target=_update(self.host), args=(), kwargs={})
        self.host_thr.run()

        self.client_thr = threading.Thread(target=_update(self.test_client), args=(), kwargs={})


    def tearDown(self):
        self.host.is_alive = False
        self.test_client.is_alive = False
        self.host_thr.join()
        self.client_thr.join()
        pass

    def test__inject_str(self):
        self.test_client._inject_str('test123')
        self.assertTrue(1==1)

    def test_log_in(self):
        logger.debug('testing log_in')

        self.test_client.send_log_in('tester')
        self.client_thr.run()



