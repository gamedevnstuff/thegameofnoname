# game dummy

basically just a network host/client dummy.
may be a good starting place for something new.


## setup

you may want to do all of that stuff in a venv.
    
    # clone enet repo
    git submodule init 
    git submodule update
    
    pip install cython
    
    # install enet to pip
    pip install -e pyenet/
    
    
then start the server, and maybe clients (but they dont do anything yet)