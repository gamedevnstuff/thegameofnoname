from .__room_generator import RoomGenerator

__author__ = "zzZZZ"


class AreaGenerator:
    """
    Generates complete area content. Ex: "surface" or "dungeon".
    """

    def __init__(self, area, dungeons):
        """
        :param area: Area modifiers to determine bounds and limits for random generation.
        :type area: dict
        :param dungeons: Dictionary of possible dungeons.
        :type dungeons: dict
        """
        MODS_KEYS = [
            "surface",
            "dungeon"
        ]
        DUNGEON_KEYS = [
            "cryptcave",
            "cabin",
            "icecave",
            "treetops"
        ]

        for key in MODS_KEYS:
            if key not in area:
                raise KeyError("INVALID area_mods: ", area)

        for key in DUNGEON_KEYS:
            if key not in dungeons:
                raise KeyError("INVALID dungeons: ", dungeons)

        self._area = area
        self._dungeons = dungeons

    def generate(self, seeder, template):
        """
        Generates a new area according to preset parameters. Including all nested levels of internal areas.

        :param seeder: The random generator that this dungeon will be based on.
        :type seeder: Seeder
        :param template: json with dungeon stats, such as: { "template": "forest", "progression": 4, "exits": "{...}" }.
        :type template: dict
        :return: A generated dungeon.
        :rtype: dict
        """
        try:
            """ Test that template is valid"""
            type = template['type']
            difficulty = template['difficulty']
            dungeons = template['dungeons']
            template = template['template']
        except KeyError:
            """ Re-raise error with specific message"""
            raise KeyError("INVALID template: ", template)

        areas = []  # TODO completed rooms go in here
        roomcount = self._get_rooms_count(seeder, type, difficulty)
        rooms = self._get_rooms_type(seeder, self._area[type]["room_balance"], roomcount)
        layout = self._get_area_layout(seeder, rooms)

        print("template: ", template)
        print("rooms: ", roomcount, rooms)

        """ Recursively generate nested areas and add them to current area's internal areas"""
        if dungeons is not False:
            for dungeon in dungeons:
                areas.append(self.generate(seeder, dungeon))

        return roomcount  # TODO should later return areas.

    def _get_rooms_count(self, seeder, type, difficulty):
        """
        Calculates number of rooms based on area type and difficulty.

        Just a number that should grow gradually depending on how far into the game a player is.

        :param seeder: Random generator.
        :type seeder: Seeder
        :param type: Area type. Ex: "surface" or "dungeon".
        :type type: string
        :param difficulty: Difficulty multiplier.
        :type difficulty: number
        :return: Room-count.
        :rtype: integer
        """
        # TODO room count probably needs refinement to scale better with difficulty-progression. Later...
        count = seeder.randint(int(difficulty * self._area[type]["floor_mod"]),
                               int(difficulty * self._area[type]["ceiling_mod"]))
        count += self._area[type]["rooms_base"]
        count = int(count * self._area[type]["rooms_mod"])
        count += seeder.randint(0, 1)
        if count > self._area[type]["rooms_max"]:
            count = self._area[type]["rooms_max"]
        return count

    def _get_rooms_type(self, seeder, balance, total):
        """
        Creates a list of room objects. Each room-object has a id, size and type.

        Ex:     { "small": 3,
                  "medium": 1,
                  "large": 2 }

        :param seeder: Random generator.
        :type seeder: Seeder
        :param balance: Room balance modifiers. Requires keys: "small", "medium" and "large".
        :type balance: dict
        :param total: The amount of rooms to generate a type for.
        :type total: integer
        :return: Amount of rooms for each category; small, medium or large.
        :rtype: dict
        """
        rooms = {}
        resummed = 0
        for key, value in balance.items():
            rooms[key] = round(total * value)
            resummed += rooms[key]

        """ Now make sure sum still adds up to original total"""
        if resummed != total:
            """ If not, start a lottery to bridge the gap"""
            """ First by only allowing types with weight above 0 to be included in the lottery"""
            allowed = []
            for key, value in balance.items():
                if value > 0:
                    allowed[key] = value

            """ Then we add the diff to some lucky winner(s)... Could be -x or +x"""
            diff = total - resummed
            plusminus = diff / abs(diff)
            altered = seeder.lottery(diff, allowed, plusminus)

            """ Add the final values back into rooms"""
            for key, value in altered.items():
                rooms[key] = value

        return rooms

    def _get_area_layout(self, seeder, rooms):
        pass
