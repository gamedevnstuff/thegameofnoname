"""
This can be our internal message system bewtween classes that require updates at runtime.
Just extend this class for any class that needs to be updated of external events, and add itself to that
Observable to be notified of any updates.
"""
class Listener:
    def __init__(self, observable):
        observable.register_observer(self)

    def notify(self, observable, *args, **kwargs):
        print('Got', args, kwargs, 'From', observable)
