import random
from libs.listener import Observable

__author__ = "zzZZZ"


class Seeder(Observable):
    """
    Wrapper for random.
    Handles the distribution of all randomized values.

    Expected usage is to supply a NEW Seeder to each component that makes use of one.
    A generator, a session, when entering a new screen, etc.

    Each time called, sets it‘s seeder with the new result.

    Pass current game-seeder, and optional tuple to generate reproducible results.
    Why a tuple? Because that is the expected ID variable from world-gen.
    For example, a region with id/coordinate (14, 56) in a given game-seeder will always generate the same content.


    :param seed: Sets the initial state of the seeder in order to generate reproducible results.
    :type seed: number
    :param coordinates: Optional param, pass a tuple: "(x, y)"; The tuple is expected to come from area id/coordinates.
    :type coordinates: tuple
    """

    def __init__(self, seed, id_tuple=False):
        super(Seeder, self).__init__()
        self.coordinates = id_tuple
        """ Either set initial seeder with a base seeder, or add values from optional tuple if present."""
        # TODO handle exception if either value inside tuple is not a number. Later...
        self.r = random.Random(seed) if id_tuple is False else random.Random(
            seed + (id_tuple[0] / 3.3) * (id_tuple[1]))
        self.state = self.r.getstate()

    def _set_state(self):
        """ Set the internal random-objects new state; incrementally from previous state"""
        self.r.setstate(self.state)
        result = self.r.random()
        self.r.seed(result)
        self.state = self.r.getstate()

    def random(self):
        """
        Generate a new (pseudo)random number.
        Always generates the same sequential numbers given that the initial seeder is the same.

        :return: Returns a float between 0 and 1.
        :rtype: number
        """
        self._set_state()
        return self.r.random()

    def randint(self, floor, ceiling):
        """
        Generate a new (pseudo)random number.
        Always generates the same sequential numbers given that the initial seeder is the same.

        :param floor: Optional param, floor is inclusive.
        :type floor: integer
        :param ceiling: Optional param, ceiling is exclusive.
        :type ceiling: integer
        :return: Any number between floor and ceiling.
        :rtype: integer
        """
        self._set_state()
        return self.r.randint(floor, ceiling)

    def choice(self, list):
        """
        Randomly select a value from a list.
        Always generates the same sequential numbers given that the initial seeder is the same.

        :param list: A list with values from which to select one.
        :type list: list
        :return: A randomly chosen element from the given list.
        """
        self._set_state()
        return self.r.choice(list)

    def lottery(self, rounds, participants, prize):
        """
        Gives out a prize-amount to a randomly selected participant, each round. Same participant can win several times.
        Helper function with random.choice at its core.

        :param rounds: How many times shall a prize be distributed?
        :type rounds: integer, negative numbers are automatically converted to positive.
        :param participants: Dictionary of available objects to receive prizes.
        :type participants: dict
        :param prize: Numerical prize, positive or negative, to be won.
        :type prize: number
        :return: Original participants, with fatter or thinner winners (fatter if pos prize, thinner if neg)
        :rtype: dict
        """
        self._set_state()
        keys = list(participants)
        for i in range(abs(rounds)):
            participants[self.r.choice(keys)] += prize
        return participants
