import json
from libs.seeder import Seeder
from libs.areagenerator import AreaGenerator

with open("data/SAMPLE_AREA.json") as f:
    preset = json.load(f)

with open("data/generator/modifier/AREA.json") as i:
    mods = json.load(i)

with open("data/generator/template/AREA.json") as l:
    dungeons = json.load(l)

SEED_INITIAL = 123456
SEED_COORDINATE = (13, 61)

x = Seeder(SEED_INITIAL, SEED_COORDINATE)

print("\n\n-- Samples from seeder: --\n")
print(x.randint(12, 151))
print(x.random())
print(x.random())
print(x.random())
print(x.randint(1, 1000))
print(x.random())

print("\n\n-- Samples from dungeon generator: --\n")
dg = AreaGenerator(mods, dungeons)
area = dg.generate(Seeder(SEED_INITIAL, SEED_COORDINATE), preset)
